package restaurante;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Empleado extends Persona {
    private Double costoHoraTrabajo;
    private ArrayList<Turno> turnosRealizados;
    private BigDecimal sueldoTotalPorTurnos;

    public Empleado(String nombre, String apellido, Double costoHoraTrabajo){
        super(nombre,apellido);
        this.costoHoraTrabajo=costoHoraTrabajo;
        this.turnosRealizados = new ArrayList<Turno>();
        this.setSueldo(new BigDecimal(0.00));
    }

    //-----------------setters-----------------

    public void setCostoHoraTrabajo(double costoHoraTrabajo){
        this.costoHoraTrabajo=costoHoraTrabajo;
    }

    public void setJornadaEmpleado(Turno turnoRealizado){
        this.turnosRealizados.add(turnoRealizado);
        this.acumularSueldoPorTurnoCompletado(turnoRealizado);
    } 

    public void setSueldo(BigDecimal sueldoInicio){
        this.sueldoTotalPorTurnos = sueldoInicio;
    }

    //---------------getters---------------------

    public double getCostoHoraTrabajo(){
        return costoHoraTrabajo;
    } 

    public ArrayList<Turno> getTurnoDelEmpleado(){
        return turnosRealizados;
    }

    public BigDecimal getSueldoTotal(){
        return sueldoTotalPorTurnos;
    }

    //-----------------------------------

    private void acumularSueldoPorTurnoCompletado(Turno turnoRealizado){
        Double sueldoDelTurno;
        sueldoDelTurno = costoHoraTrabajo * turnoRealizado.getDuracion();
        this.sueldoTotalPorTurnos = this.sueldoTotalPorTurnos.add(new BigDecimal(sueldoDelTurno));
    }

    @Override
    public String toString (){
        String nombreCompleto = "Nombre del empleado: " + getApellido() + " " + getNombre();

        String turnosCompletados = "";
        for(int i=0; i<turnosRealizados.size(); i++){
            turnosCompletados = turnosCompletados + "Ingreso: " + turnosRealizados.get(i).getHorarioDeIngreso()
            + "Salida: " + turnosRealizados.get(i).getHorarioDeSalida();
        }

        return nombreCompleto + turnosCompletados + "Sueldo Total: " + getSueldoTotal();
    }
}
