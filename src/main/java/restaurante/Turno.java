package restaurante;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Turno {

    private LocalDateTime fechaHoraInicialDelTurno;
    private LocalDateTime fechaHoraFinalDelTurno;
    private ArrayList<Empleado> empleadosEnTurno; 
    private Long duracionDelTurnoEnHoras;

    public Turno(LocalDateTime fechaHoraInicial, LocalDateTime fechaHoraFinal){
        this.fechaHoraInicialDelTurno=fechaHoraInicial;
        this.fechaHoraFinalDelTurno=fechaHoraFinal;
        this.determinarDuracion();
        empleadosEnTurno = new ArrayList<Empleado>();

    }

    //----------------setter------------------
    public void setFechaHoraInicialTurno(LocalDateTime fechaHoraInicial){
        this.fechaHoraInicialDelTurno=fechaHoraInicial;
    }

    public void setFechaHoraFinalTurno(LocalDateTime fechaHoraFinal){
        this.fechaHoraInicialDelTurno=fechaHoraFinal;
    }


    public void agregarEmpleadoATurno(Empleado empleado){
        this.empleadosEnTurno.add(empleado);
    } 

    //----------------getter------------------
    public LocalDateTime getFechaHoraInicialTurno(){
        return fechaHoraInicialDelTurno;
    }

    public LocalDateTime getFechaHoraFinalTurno(){
        return fechaHoraFinalDelTurno;
    }

    public String getHorarioDeIngreso(){

        String inicioDelTurno;
        inicioDelTurno= getFechaHoraInicialTurno().getDayOfMonth() + "/"
        + getFechaHoraInicialTurno().getMonthValue() + " " + 
        getFechaHoraInicialTurno().getHour() + ":" + getFechaHoraInicialTurno().getMinute();

        return inicioDelTurno;
    }

    public String getHorarioDeSalida(){
        String finalDelTurno;
        finalDelTurno = getFechaHoraFinalTurno().getDayOfMonth() + "/"
        + getFechaHoraFinalTurno().getMonthValue() + " " + getFechaHoraFinalTurno().getHour() + ":"
        + getFechaHoraFinalTurno().getMinute();

        return finalDelTurno;
    }

    public Long getDuracion(){
        return duracionDelTurnoEnHoras;
    }

    public ArrayList<Empleado> getEmpleadosEnTurno(){
        return empleadosEnTurno;
    }

    //------------------------------------------
    private void determinarDuracion(){
        Duration duracionDelTurno = Duration.between
        (fechaHoraInicialDelTurno.toLocalTime(), fechaHoraFinalDelTurno.toLocalTime());
        duracionDelTurnoEnHoras = duracionDelTurno.toHours();
    }

    public void mostrarEmpleadosEnTurno(){
        
        String datosInicioTurno;
        String datosFinalTurno;

        datosInicioTurno= "Inicio: " + getFechaHoraInicialTurno().getDayOfMonth() + "/"
        + getFechaHoraInicialTurno().getMonthValue() + " " + 
        getFechaHoraInicialTurno().getHour() + ":" + getFechaHoraInicialTurno().getMinute();

        datosFinalTurno= "Final: " + getFechaHoraFinalTurno().getDayOfMonth() + "/"
        + getFechaHoraFinalTurno().getMonthValue() + " " + getFechaHoraFinalTurno().getHour() + ":"
        + getFechaHoraFinalTurno().getMinute();

        System.out.println("Turno -" + datosInicioTurno + " - " + datosFinalTurno);
        if(!empleadosEnTurno.isEmpty()){
            for(int i=0; i<empleadosEnTurno.size(); i++){
                System.out.println(empleadosEnTurno.get(i).toString());
            }
        }
        else{
            System.out.println("No hay empleados registrados en este turno");
        }
        
    }
}
