package restaurante;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

//import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Test;

public class TurnoTest {

    @Test public void testUsoDeLocalDateTime(){
        
        LocalDateTime fechaHora1 = LocalDateTime.of(2020, 10, 1, 14, 10, 00);
        LocalDateTime fechaHoraEsperada = LocalDateTime.of(2020, 10, 1, 14, 10, 00);

        LocalTime horaEsperada = LocalTime.of(14,10,00);      
        
        assertEquals(fechaHoraEsperada, fechaHora1);
        assertEquals(horaEsperada, fechaHora1.toLocalTime());
    }

    @Test
    public void testDuracion(){
        LocalDateTime fechaHoraInicio = LocalDateTime.of(2020, 10, 1, 14, 10, 00);
        LocalDateTime fechaHoraFinal = LocalDateTime.of(2020, 10, 1, 15, 10, 00);

        Turno turno = new Turno(fechaHoraInicio, fechaHoraFinal);
        long duracionEsperada = 1;
        long duracionActual = turno.getDuracion();
        assertEquals(duracionEsperada, duracionActual);

    }

    @Test
    public void testCargarElArray(){

        LocalDateTime fechaHoraInicial =LocalDateTime.of(2020, 10, 3, 8, 00, 00);
        LocalDateTime fechaHoraFinal = LocalDateTime.of(2020, 10, 3, 11, 00, 00);

        Empleado empleado1 = new Empleado("Roberto", "Lopez", 1500.50);
        Empleado empleado2 = new Empleado("Juan", "Perez", 1500.50);

        Turno turno1=new Turno(fechaHoraInicial, fechaHoraFinal);
        turno1.agregarEmpleadoATurno(empleado1);
        turno1.agregarEmpleadoATurno(empleado2);

        ArrayList<Empleado> listaActual= new ArrayList<Empleado>();
        listaActual=turno1.getEmpleadosEnTurno();
        ArrayList<Empleado> listaEsperada = new ArrayList<Empleado>();
        listaEsperada.add(empleado1);
        listaEsperada.add(empleado2);

        assertArrayEquals(listaEsperada.toArray(), listaActual.toArray());
    }
}
