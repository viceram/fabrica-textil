package restaurante;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.Test;

public class EmpleadoTest {

    @Test 
    public void testCalcularSueldoTotal(){

        LocalDateTime fechaHoraInicial= LocalDateTime.of(2020, 10, 3, 8, 30, 00);
        LocalDateTime fechaHoraFinal= LocalDateTime.of(2020, 10, 3, 11, 30, 00);
        Turno turnoDelEmpleado1 = new Turno(fechaHoraInicial, fechaHoraFinal);

        LocalDateTime fechaHoraInicial2= LocalDateTime.of(2020, 10, 3, 12, 00, 00);
        LocalDateTime fechaHoraFinal2= LocalDateTime.of(2020, 10, 3, 14, 00, 00);
        Turno turnoDelEmpleado2 = new Turno(fechaHoraInicial2, fechaHoraFinal2);

        Empleado empleado1 = new Empleado("Roberto", "Lopez", 1500.50);
        empleado1.setJornadaEmpleado(turnoDelEmpleado1);
        empleado1.setJornadaEmpleado(turnoDelEmpleado2);

        BigDecimal sueldoEsperado = new BigDecimal(7502.50);   
        BigDecimal sueldoActual=empleado1.getSueldoTotal();

        assertEquals(sueldoEsperado, sueldoActual);
    }
}
